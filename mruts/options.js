// Saves options to chrome.storage
function save_options() {
	var key = document.getElementById('nextTabKey').value;
	chrome.storage.sync.set({nextTabKey: key}, 
		function() {
			var bgPage = chrome.extension.getBackgroundPage();
			if(bgPage){
				bgPage.changeSettings();
			}
			// Update status to let user know options were saved.
			var status = document.getElementById('status');
			status.textContent = 'Options saved.';
			setTimeout(
				function() {
					status.textContent = '';
				}, 750
			);
		}
	);
}

// stored in chrome.storage.
function restore_options() {
  // Use default value color = 'red' and likesColor = true.
	chrome.storage.sync.get({nextTabKey: 'cq'}, 
		function(items) {
			document.getElementById('nextTabKey').value = items.nextTabKey;
		}
	);
}
document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click', save_options);