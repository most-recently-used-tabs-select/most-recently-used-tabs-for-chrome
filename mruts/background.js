var window2tabStack=[];
var nextTab=false;
var nextTabState=false;
var stackOverRun=false;
var inject="inject.js";

const CTRL=17;
var TAB=9;

chrome.tabs.onCreated.addListener(traceTabsOnOpen);
chrome.tabs.onReplaced.addListener(traceTabsOnReplaced);
chrome.tabs.onRemoved.addListener(traceTabsOnRemoved);
chrome.tabs.onDetached.addListener(traceTabsOnDetached);
chrome.tabs.onAttached.addListener(traceTabsOnAttached);
chrome.tabs.onActivated.addListener(traceTabsOnActivate);
chrome.windows.onRemoved.addListener(traceWinOnRemoved);
//chrome.input.ime.onKeyEvent.addListener(onKeyEvent);
chrome.tabs.onUpdated.addListener(traceTabs);
chrome.runtime.onMessage.addListener( onMessage );
chrome.commands.onCommand.addListener(onCommand);
init();

function init(){
	changeSettings();
	var tabCount=0;
	chrome.windows.getAll({populate:true},
		function(windows){
			if(!windows) {
				console.log("There is no any windows?!");
				return;
			}
			for(var w=0; w < windows.length; w++){
				var win = windows[w];
				var active=false;
				if( win.id ){
					for( var t=0; t < win.tabs.length; t++ ){
						if( win.tabs[t].active) active=win.tabs[t].id;
						else {
							pushTabToStack(win.id, win.tabs[t].id)();
						}
						tabCount++;
					}
					if( active ) pushTabToStack(win.id, active)();
				} else {
					console.log("Window without ID");
				}
			}
			console.log("Init finished. " + window2tabStack.length + " windows found with " + tabCount + " Tabs on them");
		}
	);
}
function pushTabToStack(win,tab){
	return function(){
		var isFound = false;
		for(var i=0;i<window2tabStack.length && window2tabStack[i].id != win;i++);
		if( i < window2tabStack.length ){
			var stack = window2tabStack[i].stack;
			for(var j=0;j<stack.length && stack[j] != tab;j++);
			if( j < stack.length ){
				stack.splice(j,1);
				isFound = true;
			}
			stack.push(tab);
			//console.log("DEBUG: Push tab in win "+ win +". Stack length="+ window2tabStack[i].stack.length + "; tabId="+tab); 
		} else {
			window2tabStack.push({"id":win,"stack":[tab]});
			//console.log("DEBUG: Push tab in new win. tabId="+tab); 	
		}
		// Remove the same Tab from the other windows
		if( !isFound ) {
			for(var i=0;i<window2tabStack.length; i++){
				if(window2tabStack[i].id != win){
					var stack = window2tabStack[i].stack;
					for(var j=0;j<stack.length && stack[j] != tab;j++);
					if( j < stack.length ){
						stack.splice(j,1);
						//console.log("DEBUG: Tab removes from the old location");
					}
				}
			}
		}
	}
}
function addTabToStack(win,tab){
	if( win ){
		for(var i=0;i<window2tabStack.length && window2tabStack[i].id != win;i++);
		if( i < window2tabStack.length ){
			var stack = window2tabStack[i].stack;
			for(var j=0;j<stack.length && stack[j] != tab;j++);
			if( j >= stack.length ){
				stack.unshift(tab);
			}
		} else {
			window2tabStack.push({"id":win,"stack":[tab]});
		}
		//console.log("DEBUG: Add tab in win "+ i +". Stack length="+ window2tabStack[i].stack.length); 
	}
}
function removeTab(tabId){
	removeTabFromStack(false,tabId);
	/*chrome.tabs.get(tabId, 
		function( tab ){
			removeTabFromStack(tab.windowId,tabId);
		} 
	);*/
}
function removeTabFromStack(winId,tabId){
	if( winId ){
		for(var i=0;i<window2tabStack.length && window2tabStack[i].id != winId;i++);
		if( i < window2tabStack.length ){
			var stack = window2tabStack[i].stack;
			for(var j=0;j<stack.length && stack[j] != tabId;j++);
			if( j < stack.length ){
				stack.splice(j,1);
			}
			//console.log("DEBUG: Remove tab from win "+ i +". Stack length="+ window2tabStack[i].stack.length); 	
		} else {
			console.log("Window was not found"); 	
		}
	} else {
		//console.log("DEBUG: Removing Tab without window"); 	
		for(var i=0;i<window2tabStack.length;i++){
			var stack = window2tabStack[i].stack;
			for(var j=0;j<stack.length && stack[j] != tabId;j++);
			if( j < stack.length ){
				stack.splice(j,1);
				//console.log("DEBUG: Remove tab from win "+ i +". Stack length="+ window2tabStack[i].stack.length); 	
			}
		}
	}
}
function traceTabsOnOpen(tab){
	var win = tab.windowId;
	if( win ){
		//console.log("DEBUG: Open new tab in win "+ win); 
		addTabToStack(win,tab.id)
	} else {
		console.log("Tab opened without window");
	}
}
function traceTabsOnActivate(activeInfo){
	var tab = activeInfo.tabId;
	var win = activeInfo.windowId;
	if( !nextTabState && win ) {
		//console.log("DEBUG: Activate event for tab " + tab + " in win " + win); 
		pushTabToStack(win,tab)();
	} else {
		//console.log("DEBUG: Activate event disabled for " + JSON.stringify(activeInfo)); 
	}
}
function traceTabsOnReplaced(addedTabId,removedTabId){
	removeTab(removedTabId);
	chrome.tabs.get(addedTabId, 
		function( tab ){
			traceTabsOnOpen(tab);
		}
	);
}
function traceTabsOnDetached(tabId,detachInfo){
	removeTabFromStack(detachInfo.oldWindowId,tabId);
}
function traceTabsOnAttached(tabId,attachInfo){
	addTabToStack(attachInfo.newWindowId,tabId);
}
function traceTabsOnRemoved(tabId){
	removeTab(tabId);
}
function traceWinOnRemoved(windowId){
	for(var i=0;i<window2tabStack.length && window2tabStack[i].id != windowId;i++);
	if( i < window2tabStack.length ){
		window2tabStack.splice(i,1);
		//console.log("DEBUG: Remove win "+ i ); 	
	}
}

function traceTabs(tabId, changeInfo, tab){
	if( "loading"==changeInfo.status && tab.url && 
		tab.url.indexOf("chrome") != 0 && tab.url.indexOf("opera") != 0 ){
		chrome.tabs.executeScript(tabId,{file:inject, allFrames:true, runAt:"document_start"},
			function(results){
				if (chrome.runtime.lastError) {
					console.log("ERROR in inject script: " + chrome.runtime.lastError.message);
				} 
			}
		);
		//console.log("DEBUG: inject code "+inject+" into " + tab.url);
	}
}
function onMessage(request, sender, sendResponse) {
	//console.log("DEBUG: onMessage: "+JSON.stringify(request));
	if (request.nextTab){
		onKeyEvent(false,{type:"keydown",ctrlKey:true,code:TAB});
	} else if (request.stopTab) {
		onKeyEvent(false,{type:"keyup",code:CTRL});
	}
}

function onKeyEvent(engineID, keyData){
	if(keyData.type == "keydown"){
		if (keyData.code == TAB && keyData.ctrlKey) {
			//chrome.input.ime.keyEventHandled(keyData.requestId, true);
			chrome.windows.getCurrent({populate:true}, 
				function(window){
					var win = window.id;
					if( !win ) {
						//console.log("DEBUG: Active window without ID");
						return;
					}
					if(keyData.requestId) chrome.input.ime.keyEventHandled(keyData.requestId, true);
					for(var i=0;i<window2tabStack.length && window2tabStack[i].id != win;i++);
					if( i < window2tabStack.length ){
						var stack = window2tabStack[i].stack;
						var tabToActivate=false;
						if( stack.length > 1 ) {
							if( nextTabState ) {
								nextTab -= 1;
								if( nextTab < 0 ) {
									nextTab = stack.length - 1;
									stackOverRun = true;
								}
							} else {
								nextTabState = true;
								nextTab = stack.length - 2;
								stackOverRun = false;
							}
							
							tabToActivate=stack[nextTab];
						} 
						if( tabToActivate ) {
							chrome.tabs.update(tabToActivate, {active: true});
						}
						else console.log("Nothing to activate");
					} else {
						console.log("Window is not registered");
					}
					//console.log("DEBUG: Stack length="+ window2tabStack[i].stack.length + "; nextTab="+nextTab); 
				}
			);
		}
	} else if( keyData.type == "keyup"){
		if( keyData.code == CTRL ) {
			if( nextTabState ){
				chrome.windows.getCurrent({populate:true}, 
					function(window){
						var win = window.id;
						if( win ){
							for(var i=0;i<window2tabStack.length && window2tabStack[i].id != win;i++);
							if( i < window2tabStack.length ){
								var stack = window2tabStack[i].stack;
								if( stack.length > 0 ){
									if( stackOverRun ){
										window2tabStack[i].stack = 
											stack.slice(0,nextTab).reverse().concat(
											stack.slice(nextTab,stack.length).reverse());
									} else {
										window2tabStack[i].stack = 
											stack.slice(0,nextTab).concat(
											stack.slice(nextTab,stack.length).reverse());
									}
								} else {
									console.log("Error: stack is empty");
								}
							} else {
								console.log("Window is not registered"); 
							}
							//console.log("DEBUG: stopTab Stack length="+ window2tabStack[i].stack.length); 
							nextTab=false;
							nextTabState=false;
							stackOverRun = false;
						} else {
							console.log("Window has no ID"); 
						}
					}
				);
			}
		}
	}
}

function changeSettings(){
	chrome.storage.sync.get({nextTabKey: 'ct'}, 
		function(items) {
			switch(items.nextTabKey){
				case "cq":
					TAB = 192;
					inject = "inject_cq.js";
					break;
				default:
					TAB = 9;
					inject = "inject.js";
			}
		}
	);
}

function onCommand(command) {
	//console.log("DEBUG: command:", command);
	onKeyEvent(false,{type:"keydown",ctrlKey:true,code:TAB});
}